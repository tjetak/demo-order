// External Dependancies
const boom = require('boom')
const {
	models: {
		Order
	}
} = require('demo-model')
const { 
	lib: {
		PubSub
	}
} = require('demo-lib')

// Get Data Models

// Get all products
exports.getOrders = async () => {
	try {
		const orders = await Order.find()
		return orders
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.getSingleOrder = async req => {
	try {
		const id = req.params === undefined ? req.id : req.params.id
		const order = await Order.findById(id)
		return order
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.addOrder = async req => {
	try {
		console.log("i am inside add order")
		const order = new Order(req)
		const newOrder = await order.save()
		PubSub.publishMessage("orders-topic", JSON.stringify(newOrder.toJSON()))
		return newOrder
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.updateOrder = async req => {
	try {
		const id = req.params === undefined ? req.id : req.params.id
		const updateData = req.params === undefined ? req : req.params
		const update = await Order.findByIdAndUpdate(id, updateData, { new: true })
		return update
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.deleteOrder = async req => {
	try {
		const id = req.params === undefined ? req.id : req.params.id
		const order = await Order.findByIdAndRemove(id)
		return order
	} catch (err) {
		throw boom.boomify(err)
	}
}
