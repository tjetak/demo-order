// External Dependancies
const boom = require('boom')
const {
	models: {
		Product
	}
} = require('demo-model')

// Get Data Models

// Get all products
exports.getProducts = async () => {
	try {
		const products = await Product.find()
		return products
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.getSingleProduct = async req => {
	try {
		const id = req.params === undefined ? req.id : req.params.id
		const product = await Product.findById(id)
		return product
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.addProduct = async req => {
	try {
		const product = new Product(req)
		const newProduct = await product.save()
		return newProduct
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.updateProduct = async req => {
	try {
		const id = req.params === undefined ? req.id : req.params.id
		const updateData = req.params === undefined ? req : req.params
		const update = await Product.findByIdAndUpdate(id, updateData, { new: true })
		return update
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.deleteProduct = async req => {
	try {
		const id = req.params === undefined ? req.id : req.params.id
		const product = await Product.findByIdAndRemove(id)
		return product
	} catch (err) {
		throw boom.boomify(err)
	}
}
