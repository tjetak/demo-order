// Import our Controllers
const productController = require('../controllers/productController')
const orderController = require('../controllers/orderController')

const routes = [
  {
    method: 'GET',
    url: '/api/products',
    handler: productController.getProducts
  },
  {
    method: 'GET',
    url: '/api/products/:id',
    handler: productController.getSingleProduct
  },
  {
    method: 'POST',
    url: '/api/products',
    handler: productController.addProduct,
    // schema: documentation.addCarSchema
  },
  {
    method: 'PUT',
    url: '/api/products/:id',
    handler: productController.updateProduct
  },
  {
    method: 'DELETE',
    url: '/api/products/:id',
    handler: productController.deleteProduct
  },
  //ORDER
  {
    method: 'GET',
    url: '/api/orders',
    handler: orderController.getOrders
  },
  {
    method: 'GET',
    url: '/api/orders/:id',
    handler: orderController.getSingleOrder
  },
  {
    method: 'POST',
    url: '/api/orders',
    handler: orderController.addOrder,
  },
  {
    method: 'PUT',
    url: '/api/orders/:id',
    handler: orderController.updateOrder
  },
  {
    method: 'DELETE',
    url: '/api/orders/:id',
    handler: orderController.deleteOrder
  },
]

module.exports = routes
