// Import External Dependancies
const graphql = require('graphql')

// Destructure GraphQL functions
const {
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLID,
	GraphQLList,
	GraphQLNonNull
} = graphql

// Import Controllers
const productController = require('../controllers/productController')
const orderController = require('../controllers/orderController')

// Define Object Types

const productType = new GraphQLObjectType({
	name: 'Product',
	fields: () => ({
		_id: { type: GraphQLID },	
		productName: { type: GraphQLString },
		productCode: { type: GraphQLString },
		productDescription: { type: GraphQLString },
	})
})

const orderType = new GraphQLObjectType({
	name: 'Order',
	fields: () => ({
		_id: { type: GraphQLID },	
		amount: { type: GraphQLInt },
		quantity: { type: GraphQLString },
	})
})

// Define Root Query
const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		product: {
			type: productType,
			args: { id: { type: GraphQLID } },
			async resolve(parent, args) {
				return await productController.getSingleProduct(args)
			}
		},
		products: {
			type: new GraphQLList(productType),
			async resolve(parent, args) {
				return await productController.getProducts()
			}
		},
		order: {
			type: orderType,
			args: { id: { type: GraphQLID } },
			async resolve(parent, args) {
				return await orderController.getSingleOrder(args)
			}
		},
		orders: {
			type: new GraphQLList(orderType),
			async resolve(parent, args) {
				return await orderController.getOrders()
			}
		},
		
	}
})

// Define Mutations
const Mutations = new GraphQLObjectType({
	name: 'Mutations',
	fields: {
		addProduct: {
			type: productType,
			args: {
				productName: { type: new GraphQLNonNull(GraphQLString) },
				productCode: { type: new GraphQLNonNull(GraphQLString) },
				productDescription: { type: GraphQLString },
			},
			async resolve(parent, args) {
				const data = await productController.addProduct(args)
				return data
			}
		},
		editProduct: {
			type: productType,
			args: {
				id: { type: new GraphQLNonNull(GraphQLID) },
				productName: { type: new GraphQLNonNull(GraphQLString) },
				productCode: { type: new GraphQLNonNull(GraphQLString) },
				productDescription: { type: GraphQLString },
			},
			async resolve(parent, args) {
				const data = await productController.updateProduct(args)
				return data
			}
		},
		deleteProduct: {
			type: productType,
			args: {
				id: { type: new GraphQLNonNull(GraphQLID) }
			},
			async resolve(parent, args) {
				const data = await productController.deleteProduct(args)
				return data
			}
		},
		// ORDER
		addOrder: {
			type: orderType,
			args: {
				amount: { type: new GraphQLNonNull(GraphQLInt) },
				quantity: { type: new GraphQLNonNull(GraphQLString) },
			},
			async resolve(parent, args) {
				const data = await orderController.addOrder(args)
				return data
			}
		},
		editOrder: {
			type: orderType,
			args: {
				id: { type: new GraphQLNonNull(GraphQLID) },
				amount: { type: new GraphQLNonNull(GraphQLInt) },
				quantity: { type: new GraphQLNonNull(GraphQLString) },
			},
			async resolve(parent, args) {
				const data = await orderController.updateOrder(args)
				return data
			}
		},
		deleteOrder: {
			type: orderType,
			args: {
				id: { type: new GraphQLNonNull(GraphQLID) }
			},
			async resolve(parent, args) {
				const data = await orderController.deleteOrder(args)
				return data
			}
		}

	}
})

// Export the schema
module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: Mutations
})
